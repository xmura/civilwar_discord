module xo/civilwar

go 1.13

require (
	github.com/Songmu/go-httpdate v1.0.0 // indirect
	github.com/bwmarrin/discordgo v0.22.0
	github.com/carlescere/scheduler v0.0.0-20170109141437-ee74d2f83d82 // indirect
	github.com/go-gorp/gorp v2.2.0+incompatible
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/mattn/go-sqlite3 v1.14.2
	golang.org/x/crypto v0.0.0-20200728195943-123391ffb6de // indirect
	golang.org/x/sys v0.0.0-20200806060901-a37d78b92225 // indirect
)
