package config

import (
	"encoding/json"
	"io/ioutil"
)

type Config struct {
	Server Server
	System System
}

type Server struct {
	Id string
	EntryChannelId string
	InformationChannelId string
	AnnounceChannelId string
}

type System struct {
	TournamentCheckInterval int
}

func Load() Config {
	var config Config

	bytes, readErr := ioutil.ReadFile("config.json")
	if readErr != nil {
		return config
	}

	jsonErr := json.Unmarshal(bytes, &config)
	if jsonErr != nil {
		return config
	}

	return config
}

