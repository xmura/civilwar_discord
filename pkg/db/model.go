package db

import (
	"time"
)

type Tournament struct {
	Id int64 `db:"id, primarykey, autoincrement"`
	Title string `db:"title"`
	Datetime time.Time `db:"datetime"`
}

type Entry struct {
	Id int64 `db:", primarykey, autoincrement"`
	TournamentId int64 `db:"tournament_id"`
	DiscordUserId string `db:"discord_user_id"`
	Name string `db:"name"`
	Comment string `db:"commnet"`
}

type EntryMessage struct {
	Id int64 `db:", primarykey, autoincrement"`
	TournamentId int64 `db:"tournament_id"`
	EntryId int64 `db:"entry_id"`
	DiscordMessageId string `db:"discord_message_id"`
}

//Tournament methods
func GetNearestTournament() (*Tournament, error) {
	tournaments := []Tournament{}
	_, err := Select(&tournaments, "select * from tournaments where datetime > datetime(CURRENT_TIMESTAMP, '+9 hours') order by datetime, id limit 1")
	if err != nil || len(tournaments) == 0 {
		return nil, err
	}

	return &tournaments[0], nil
}

func (tournament *Tournament) GetEntryDeadline() time.Time {
	return tournament.Datetime.Add(-10 * time.Minute)
}

//Entry methods
func GetEntry(tournament *Tournament, userId string) (*Entry, error) {
	entries := []Entry{}
	_, err := Select(&entries,
		"select * from entries where tournament_id = ? and  discord_user_id = ?",
		tournament.Id,
		userId,
	)
	if err != nil || len(entries) == 0 {
		return nil, err
	}

	return &entries[0], nil
}

func GetEntries(tournament *Tournament) ([]Entry, error) {
	entries := []Entry{}
	_, err := Select(&entries,
		"select * from entries where tournament_id = ?",
		tournament.Id,
	)
	if err != nil {
		return nil, err
	}

	return entries, nil
}

func GetEntryCount(tournament *Tournament) (int64, error) {
	count, err := dbMap.SelectInt(
		"select count(*) from entries where tournament_id = ?",
		tournament.Id,
	)
	if err != nil {
		return 0, err
	}

	return count, nil
}

//Entry methods
func GetEntryMessages(entry *Entry) ([]EntryMessage, error) {
	messages := []EntryMessage{}
	_, err := Select(&messages,
		"select * from entry_messages where entry_id = ?",
		entry.Id,
	)
	if err != nil {
		return nil, err
	}

	return messages, nil
}
