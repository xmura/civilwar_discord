package db

import (
	"database/sql"

	"github.com/go-gorp/gorp"
	_ "github.com/mattn/go-sqlite3"
)

var db *sql.DB
var dbMap *gorp.DbMap

func init() {
	var err error
	db, err = sql.Open("sqlite3", "./data.db")
	if err != nil {
		panic(err)
	}
	dbMap = &gorp.DbMap{Db: db, Dialect: gorp.SqliteDialect{}}
	
	//initialize
	dbMap.AddTableWithName(Tournament{}, "tournaments")
	dbMap.AddTableWithName(Entry{}, "entries")
	dbMap.AddTableWithName(EntryMessage{}, "entry_messages")

	err = dbMap.CreateTablesIfNotExists()
	if err != nil {
		panic(err)
	}
}

func Close() {
	dbMap.Db.Close()
}

func Query(query string) (*sql.Rows, error) {
	return dbMap.Query(query)
}

func Select(models interface{}, query string, args ...interface{}) ([]interface{}, error) {
	return dbMap.Select(models, query, args...)
}

func Insert(models ...interface{}) error {
	return dbMap.Insert(models...)
}

func Update(model interface{}) (int64, error) {
	return dbMap.Update(model)
}
