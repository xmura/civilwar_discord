// Declare this file to be part of the main package so it can be compiled into
// an executable.
package main

// Import all Go packages required for this file.
import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/Songmu/go-httpdate"
	"github.com/bwmarrin/discordgo"
	"github.com/carlescere/scheduler"

	"xo/civilwar/pkg/config"
	"xo/civilwar/pkg/db"
)

const Version = "v0.0.0"
const timeFormat = "2006-01-02 15:04"

var Config = config.Load()

// Session is declared in the global space so it can be easily used
// throughout this program.
// In this use case, there is no error that would be returned.
var Session, _ = discordgo.New()

// Read in all configuration options from both environment variables and
// command line arguments.
func init() {

	// setting environment
	loc, locErr := time.LoadLocation("Asia/Tokyo")
	if locErr == nil {
		time.Local = loc
	}
	
	// Discord Authentication Token
	Session.Token = os.Getenv("DG_TOKEN")
	if Session.Token == "" {
		flag.StringVar(&Session.Token, "t", "", "Discord Authentication Token")
	}
}

func main() {
	//print(Config.Server.AnnounceChannelId)

	defer onPanic()

	// Declare any variables needed later.
	var err error

	// Print out a fancy logo!
	fmt.Printf(`start bot server %s`+"\n\n", Version)

	// Parse command line arguments
	flag.Parse()

	Session.Token = "Bot " + Session.Token

	// Verify a Token was provided
	if Session.Token == "" {
		log.Println("You must provide a Discord authentication token.")
		return
	}

	Session.AddHandler(onMessage)

	//Session.Identify.Intents = discordgo.MakeIntent(discordgo.IntentsGuildMessages)

	// Open a websocket connection to Discord
	err = Session.Open()
	if err != nil {
		log.Printf("error opening connection to Discord, %s\n", err)
		os.Exit(1)
	}

	// Register Scheduler
	scheduler.Every(Config.System.TournamentCheckInterval).Minutes().Run(onStartTournament)

	// Wait for a CTRL-C
	log.Printf(`Now running. Press CTRL-C to exit.`)
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Clean up
	defer func() {
		Session.Close()
		db.Close()
	}()

	// Exit Normally.
}

func onPanic() {
	err := recover()
	if err != nil {
		log.Fatal(err)
	}
}

func onError(err error) {
	log.Fatal(err)
}

func onMessage(s *discordgo.Session, message *discordgo.MessageCreate) {

	//有効なメッセージかチェック
	if message.Author.Bot {
		return
	}

	// if len(m.Mentions) == 0 {
	// 	return
	// }

	// for _, user := range m.Mentions {
	// 	if user.ID != s.State.User.ID {
	// 		return
	// 	}
	// }

	channel, chanelErr := s.State.Channel(message.ChannelID)
	if chanelErr != nil {
		onError(chanelErr)
		return
	}
	
	//処理の分岐
	if channel.Type == discordgo.ChannelTypeDM {
		//DMだったらエントリー受付
		acceptEntry(s, channel, message.Message)
		return
	} else {
		//それ以外は大会作成処理
		createTournament(s, channel, message.Message)
	}

	//sendMessage(s, Config.Server.AnnounceChannelId, fmt.Sprintf(m.Author.Mention()+" "+m.Author.Username+" "+attachment.URL))
}

func createTournament(s *discordgo.Session, channel *discordgo.Channel, message *discordgo.Message){

	commands := strings.Split(message.Content, ",")

	//有効なメッセージかチェック
	if len(message.Mentions) == 0 {
		return
	}
	for _, user := range message.Mentions {
		if user.ID != s.State.User.ID {
			return
		}
	}

	if len(commands) != 2 {
		return
	}

	loggingOnMessage(message)

	//送信元を取得
	userId := message.Author.ID
	senderName := message.Author.Username
	member, err := s.GuildMember(Config.Server.Id, userId)
	if err == nil && member.Nick != "" {
		senderName = member.Nick
	}

	replyChannelId := message.ChannelID

	//大会を作成
	title := commands[0]
	title = strings.Split(commands[0], " ")[1]
	title = strings.TrimSpace(title)

	datetime, dateErr := httpdate.Str2Time(commands[1], nil)
	if dateErr != nil {
		sendMessage(s, replyChannelId, "登録失敗：無効な日時")
		return
	}
	duration := datetime.Sub(time.Now()).Hours()
	//fmt.Println(duration)
	if duration < 1 {
		sendMessage(s, replyChannelId, "登録失敗：開催日時は現在時刻から1時間以上あとにしてください")
		return
	}

	tournament := &db.Tournament {
		Title: title,
		Datetime: datetime,
	}

	saveError := db.Insert(tournament)
	if saveError != nil {
		sendMessage(s, replyChannelId, "大会保存エラー：" + saveError.Error())
		return
	}

	infomationText := fmt.Sprintf(
		"「%s」%s 開催 (エントリー締め切り:%s) by %s",
		tournament.Title,
		tournament.Datetime.Format(timeFormat),
		tournament.GetEntryDeadline().Format(timeFormat),
		senderName,
	)

	sendMessage(s, replyChannelId, "大会登録完了！")
	sendMessage(s, Config.Server.InformationChannelId, infomationText)
}

func acceptEntry(s *discordgo.Session, channel *discordgo.Channel, message *discordgo.Message){
	//有効なメッセージかチェック
	if len(message.Attachments) == 0 {
		return
	}

	loggingOnMessage(message)

	//送信元を取得
	userId := message.Author.ID
	senderName := message.Author.Username
	member, err := s.GuildMember(Config.Server.Id, userId)
	if err == nil && member.Nick != "" {
		senderName = member.Nick
	}

	replyChannel := channel

	//直近の大会を取得
	tournament, tournamentErr := db.GetNearestTournament()
	if tournamentErr != nil {
		sendMessage(s, replyChannel.ID, "大会取得エラー："+tournamentErr.Error())
		return
	}
	if tournament == nil {
		sendMessage(s, replyChannel.ID, "エントリー失敗：大会の予定はありません")
		return
	}

	//既にエントリー済みか確認
	currentEntry, entryErr := db.GetEntry(tournament, userId)
	if entryErr != nil {
		sendMessage(s, replyChannel.ID, "エントリー確認エラー："+entryErr.Error())
		return
	}

	//保存
	entry := &db.Entry{
		TournamentId: tournament.Id,
		DiscordUserId: userId,
		Name: senderName,
		Comment: message.Content,
	}

	var saveError error
	if currentEntry == nil {
		saveError = db.Insert(entry)
	} else {
		entry.Id = currentEntry.Id
		_, saveError = db.Update(entry)
	}
	if saveError != nil {
		sendMessage(s, replyChannel.ID, "エントリー保存エラー："+saveError.Error())
		return
	}

	entry_message := &db.EntryMessage{
		TournamentId: tournament.Id,
		EntryId: entry.Id,
		DiscordMessageId: message.ID,
	}
	saveError = db.Insert(entry_message)
	if saveError != nil {
		sendMessage(s, replyChannel.ID, "エントリー保存エラー："+saveError.Error())
		return
	}

	sendMessage(s, replyChannel.ID, "エントリー完了！")

	if currentEntry == nil {
		count, _ := db.GetEntryCount(tournament)

		infomationText := fmt.Sprintf(
			"%sさん が 「%s」 にエントリーしました！ (現在%d人)",
			entry.Name,
			tournament.Title,
			count)

		sendMessage(s, Config.Server.InformationChannelId, infomationText)
	}
}

func loggingOnMessage(message *discordgo.Message){
	//送信元を取得
	senderName := fmt.Sprintf("%s(%s)", message.Author.Username, message.Author.ID)
	fmt.Printf("< %s by %s on %s\n", message.Content, senderName, message.ChannelID)
}

func sendMessage(session *discordgo.Session, channelId string, message string ){
	_, err := session.ChannelMessageSend(channelId, message)
	if err != nil {
		onError(err)
	}
}

func onStartTournament() {
	session := Session

	//直近の大会を取得
	tournament, tournamentErr := db.GetNearestTournament()
	if tournamentErr != nil {
		sendMessage(session, Config.Server.InformationChannelId, "大会取得エラー："+tournamentErr.Error())
		return
	}
	if tournament == nil {
		return
	}
	
	//時間チェック
	datetime := tournament.Datetime
	duration := datetime.Sub(time.Now()).Minutes()
	//fmt.Println(duration)
	if duration > float64(Config.System.TournamentCheckInterval) {
		return
	}

	//エントリー内容を投稿
	tournamentText := fmt.Sprintf(
		"「%s」のエントリー内容を発表します！",
		tournament.Title,
	)
	sendMessage(session, Config.Server.AnnounceChannelId, tournamentText)

	entries, entryErr := db.GetEntries(tournament)
	if entryErr != nil {
		sendMessage(session, Config.Server.InformationChannelId, "エントリー取得エラー："+entryErr.Error())
	}

	for i, entry := range entries {
		mentionStr := ""
		dmChannelId := ""
		urlsStr := ""

		//ユーザー情報とDMチャンネルを取得
		user, userErr := session.User(entry.DiscordUserId)
		if userErr == nil {
			mentionStr = user.Mention()
			dmChannel, channelError := session.UserChannelCreate(user.ID)

			if channelError == nil {
				dmChannelId = dmChannel.ID
			} else {
				urlsStr = "画像取得失敗"
			}
		} else {
			mentionStr = "ユーザー取得失敗"
		}

		//DMから画像URLを取得
		if dmChannelId != "" {
			entryMessages, emErr := db.GetEntryMessages(&entry)
			if emErr == nil {
				builder := strings.Builder{}
				for _, entryMessage := range entryMessages {
					message, messagerErr := session.ChannelMessage(dmChannelId, entryMessage.DiscordMessageId)
					if messagerErr == nil {
						for _, attachment := range message.Attachments{
							builder.WriteString(attachment.URL)
							builder.WriteString(" ")
						}
					} else {
						//メッセージ取得できない＝画像差し替え目的などでDMを削除されたとみなして無視する
					}
				}
				urlsStr = builder.String()
			} else {
				urlsStr = "画像取得失敗"
			}
		}

		entryText := fmt.Sprintf(
			"エントリーNo.%d %s(%s)\n%s\n%s",
			i+1,
			entry.Name,
			mentionStr,
			entry.Comment,
			urlsStr,
		)
		sendMessage(session, Config.Server.AnnounceChannelId, entryText)		
	}

	sendMessage(session, Config.Server.AnnounceChannelId, "以上！")
}
